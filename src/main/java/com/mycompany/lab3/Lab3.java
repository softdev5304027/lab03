/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab3 {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'},};
    private static char Player = 'X';
    private static int row;
    private static int col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printTable();
            printPlayer();
            inputRowCol();
            if (Win()) {
                printTable();
                printWinner();
                break;
            }
            changePlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private static void printPlayer() {
        System.out.println("Player : " + Player);
    }

    private static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please input Row: ");
            row = kb.nextInt();
            System.out.print("Please input Col: ");
            col = kb.nextInt();
            if (row <= 0 || col <= 0 || row > 3 || col > 3) {
                System.out.println("This position is off the bounds of the board! Try again.");
            } else {
                if (table[row - 1][col - 1] == '-') {
                    table[row - 1][col - 1] = Player;
                    return;
                }
            }

        }
    }

    private static void changePlayer() {
        if (Player == 'X') {
            Player = 'O';
        } else {
            Player = 'X';
        }
    }

    private static boolean Win() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println("Congratulation");
        System.out.println("Player " + Player + " " + "Win");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != Player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != Player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (table[i][i] != Player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (table[i][table.length - 1 - i] != Player) {
                return false;
            }
        }
        return true;
    }

    
}
