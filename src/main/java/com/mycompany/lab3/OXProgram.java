/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab3;

/**
 *
 * @author informatics
 */
class OXProgram {

    static boolean checkWin(String[][] table, String currentPlayer) {
        if (checkRow(table, currentPlayer, 0)) {
            return true;
        }
        if (checkRow(table, currentPlayer, 1)) {
            return true;
        }
        if (checkRow(table, currentPlayer, 2)) {
            return true;
        }
        if (checkCol(table, currentPlayer, 0)) {
            return true;
        }
        if (checkCol(table, currentPlayer, 1)) {
            return true;
        }
        if (checkCol(table, currentPlayer, 2)) {
            return true;
        }
        if (checkX1(table, currentPlayer, 0)) {
            return true;
        }
        if (checkX2(table, currentPlayer, 0)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String currentPlayer, int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }

    private static boolean checkCol(String[][] table, String currentPlayer, int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }

    private static boolean checkX1(String[][] table, String currentPlayer, int X1) {
        return table[X1][X1].equals(currentPlayer) && table[X1 + 1][X1 + 1].equals(currentPlayer) && table[X1 + 2][X1 + 2].equals(currentPlayer);
    }

    private static boolean checkX2(String[][] table, String currentPlayer, int X2) {
        return table[0][X2 + 2].equals(currentPlayer) && table[1][X2 + 1].equals(currentPlayer) && table[2][X2].equals(currentPlayer);
    }

    static Object checkDraw(String[][] table) {
        return true;
    }
}
